import './App.css';
import React from 'react';

import DigitalClock from './Components/DigitalClock/DigitalClock';
import Countdown from './Components/Countdown/Countdown';
import Stopwatch from './Components/Stopwatch/Stopwatch';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <DigitalClock/>
        <Countdown/>
        <Stopwatch/>
      </header>
    </div>
  );
}

export default App;
