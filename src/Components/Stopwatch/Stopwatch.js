import React, { useEffect, useState } from "react";
import './Stopwatch.css';

const Stopwatch = () => {
    const [time, setTime] = useState(0);
    const [timerOn, setTimerOn] = useState(false);
  
    useEffect(() => {
      let interval = null;
  
      if (timerOn) {
        interval = setInterval(() => {
          setTime((prevTime) => prevTime + 10);
        }, 10);
      } else {
        clearInterval(interval);
      }
  
      return () => clearInterval(interval);
    }, [timerOn]);
    return (
      <div className="stopwatchContainer">
        <div className="stopwatchText">
          <h1>My Stopwatch</h1>
        </div>
        <div className="stopwatchDivGeral">
          <div className="stopwatchkDiv">
            <div className="btnClockPretendStopwatch"></div>
            <h2>
              <span>{("0" + Math.floor((time / 60000) % 60)).slice(-2)}:</span>
              <span>{("0" + Math.floor((time / 1000) % 60)).slice(-2)}:</span>
              <span>{("0" + ((time / 10) % 100)).slice(-2)}</span>
            </h2>
          </div>
          <div id="buttonsStopwatch">
            {!timerOn && time === 0 && (
            <button onClick={() => setTimerOn(true)}>Start</button>
            )}
            {timerOn && <button onClick={() => setTimerOn(false)}>Stop</button>}
            {!timerOn && time > 0 && (
            <button className="stopwatchResetBtn" onClick={() => setTime(0)}>Reset</button>
            )}
            {!timerOn && time > 0 && (
            <button onClick={() => setTimerOn(true)}>Resume</button>
            )}
          </div>
        </div>
      </div>
    );
  };
  
export default Stopwatch;