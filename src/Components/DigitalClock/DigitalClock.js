import React, { useEffect, useState } from "react";
import './DigitalClock.css';

const DigitalClock = () => {
  const [clockState, setClockState] = useState();

  useEffect(() => {
    setInterval(() => {
      const date = new Date();
      setClockState(date.toLocaleTimeString());
    }, 1000);
  }, []);
  return (
    <div className="digitalClockContainer">
      <div className="digitalClockText">
        <h1>My Digital Clock</h1>
      </div>
      <div className="digitalClockDiv">
        <div className="btnClockPretend"></div>
        <h2>{clockState}</h2>
      </div>
    </div>
  );
};

export default DigitalClock;